package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;

import java.util.List;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST USERS]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @NotNull final List<UserDTO> usersDTO = serviceLocator.getAdminUserEndpoint().findAllUsers(sessionDTO);
            int index = 1;
            for (@NotNull final UserDTO userDTO: usersDTO) {
                System.out.println(index + ". " + userDTO.getId() + ": " + userDTO.getLogin());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
