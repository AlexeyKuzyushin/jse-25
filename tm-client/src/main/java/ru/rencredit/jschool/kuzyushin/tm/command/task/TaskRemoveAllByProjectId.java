package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class TaskRemoveAllByProjectId extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-all-by-projectId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks of project";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER PROJECTID:");
        if (serviceLocator != null) {
            @Nullable final String projectId = TerminalUtil.nextLine();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().removeAllTasksByProjectId(sessionDTO, projectId);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
