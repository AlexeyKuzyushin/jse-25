package ru.rencredit.jschool.kuzyushin.tm.constant;

public class DataConstant {

    public final static String FILE_BINARY = "./data.bin";

    public final static String FILE_BASE64 = "./data.base64";

    public final static String FILE_JSON = "./data.json";

    public final static String FILE_XML = "./data.xml";
}
